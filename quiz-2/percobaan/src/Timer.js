import React, {Component} from 'react'

class Timer extends Component{
  constructor(props){
    super(props)
    this.state = {
      time: 5,
      showTime: true
    }
  }

  componentDidMount(){
    this.timerID = setInterval(
      () => this.setState(({time}) => ({time : time - 1})) ,
      1000
    );
  }

  componentDidUpdate(){
      if (this.state.time === 0){
          this.state.showTime = false;
      }

  }

  componentWillUnmount(){
    clearInterval(this.timerID);
  }

//   tick() {
//     this.setState({
//       time: this.state.time - 1 
//     });
//   }

  stopTime(){
    this.componentWillUnmount()
  }

 //hideTime(){
   // this.setState({showTime: false})
  //}


  render(){
    return (
      <>
        {
          this.state.showTime && (
            <h1 style={{textAlign: "center"}}>
              {this.state.time}
              <br/>
            </h1>
          )
        }
      </>
    )

  }
}


export default Timer